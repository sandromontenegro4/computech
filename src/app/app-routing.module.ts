import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path:'landing',
    loadChildren: () => import ('./landing/landing.module')
    .then(modulo => modulo.LandingModule)
  },
  {
    path:'clientes',
    loadChildren:() => import ('./clientes/clientes.module')
    .then(modulo => modulo.ClientesModule)
  },
  {
    path: 'productos',
    loadChildren: () => import ('./productos/productos.module')
    .then(modulo => modulo.ProductosModule)
  },
  {
    path: '',
    redirectTo: 'landing',
    pathMatch:'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
