import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductoCreateComponent } from './producto-create/producto-create.component';
import { ProductoListComponent } from './producto-list/producto-list.component';
import { ProductoUpdateComponent } from './producto-update/producto-update.component';
import { ProductoViewComponent } from './producto-view/producto-view.component';
import { productosRoutingModule } from './productos-routing.module';



@NgModule({
  declarations: [
    ProductoCreateComponent,
     ProductoListComponent,
      ProductoUpdateComponent,
       ProductoViewComponent],
  imports: [
    CommonModule,
    productosRoutingModule
  ]
})
export class ProductosModule { }
