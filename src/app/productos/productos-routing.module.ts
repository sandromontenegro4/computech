import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClienteCreateComponent } from '../clientes/cliente-create/cliente-create.component';
import { ProductoCreateComponent } from './producto-create/producto-create.component';
import { ProductoListComponent } from './producto-list/producto-list.component';
import { ProductoUpdateComponent } from './producto-update/producto-update.component';
import { ProductoViewComponent } from './producto-view/producto-view.component';


const routes: Routes = [
  {
    path:'producto-create',
    component: ProductoCreateComponent
  },
  {
    path:'producto-list',
    component: ProductoListComponent
  },
  {
   path: 'producto-update',
   component: ProductoUpdateComponent
  },
  {
    path: 'producto-view',
    component: ProductoViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class productosRoutingModule { }
