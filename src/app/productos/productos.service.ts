import { Injectable } from '@angular/core';
import { HttpClient } from 'selenium-webdriver/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductosService {

  constructor(private http: HttpClient) { }
 public query(): Observable<ProductosService[]>{
   return this.http.get<ProductosService[]>('${environment.END_POINT}api/')
   .pipe(map(res =>{
     return res;
   }))
   }
   
 }

}
