import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClienteCreateComponent } from './cliente-create/cliente-create.component';
import { ClienteListComponent } from './cliente-list/cliente-list.component';
import { ClienteUpdateComponent } from './cliente-update/cliente-update.component';
import { ClienteViewComponent } from './cliente-view/cliente-view.component';
import { clienteRoutingModule } from './clientes-routing.module';



@NgModule({
  declarations: [ClienteCreateComponent, ClienteListComponent, ClienteUpdateComponent, ClienteViewComponent],
  imports: [
    CommonModule,
    clienteRoutingModule
  ]
})
export class ClientesModule { }
