import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ClientesService {

  constructor(private http: HttpClient) { }
  
  public query(): Observable<ClientesService[]>{
    return this.http.get<ClientesService[]>(`${environment.ENDPOINT}api/client`)
   .pipe(map(res =>{
     return res; 
   }))

  }

}
