import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClienteCreateComponent } from './cliente-create/cliente-create.component';
import { ClienteListComponent } from './cliente-list/cliente-list.component';
import { ClienteUpdateComponent } from './cliente-update/cliente-update.component';
import { ClienteViewComponent } from './cliente-view/cliente-view.component';


const routes: Routes = [
  {
    path: 'cliente-create',
    component: ClienteCreateComponent
  },
  {
    path:'cliente-list',
    component: ClienteListComponent
  },
  {
   path:'cliente-update',
   component: ClienteUpdateComponent
  },
  {
    path:'cliente-view',
    component: ClienteViewComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class clienteRoutingModule { }
